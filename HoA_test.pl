use strict;
use warnings;
use Data::Dumper;
use Sort::Naturally;

my %HoA = (  CAT =>[ 
                    [ qw ( white black red ) ],
                    [ qw ( blue green yellow ) ]
                  ],
          );

print Dumper \%HoA;

my $name = "CAT";
print $HoA{$name}->[1][1], "\n";
print $HoA{$name}[1][2], "\n";

push $HoA{$name},  [ qw ( pink green red) ];
print Dumper \%HoA;


print "---------------------------------\n";


my %HoAt;

print "name: $name\n";
if ( $HoAt{$name} ){
     print "\$HoAt{$name} Exists!\n";
   }

$HoAt{$name} = [];

push $HoAt{$name}, [ qw ( white black red ) ];
push $HoAt{$name}, [ qw ( blue green yellow ) ];

print Dumper \%HoAt;

my $hash_ref = \%HoAt;


print_hash_ref("Print href",$hash_ref);

# sub
#
sub print_hash_ref{
my($title,$href) = @_;
print "-------< $title >-------\n";
foreach my $tag ( nsort keys %{ $href } ){
    print "\$tag: $tag\n";
     foreach my $ii ( 0 .. $#{ ${$href}{$tag} } ){
               # print "[$ii]";
          foreach my $kk ( 0 .. $#{ ${$href}{$tag}[$ii] } ){
                 print "[$ii][$kk]:";
                 print "${$href}{$tag}[$ii][$kk]\n";
                };
               };
             };
};



