use strict;
use warnings;
use Data::Dumper;
use Sort::Naturally;

###

my $budget_file =$ARGV[0];
my $entries_file =$ARGV[1];

print "\n";

my($hash_ref,$header_ref) = open_files_two_col("Budget",$budget_file);
print_hash("Printing budget",$hash_ref);

my $entry_hash_ref = open_files_one_col("Making Entries",$entries_file);
print_hash("Printing Entry Totals",$entry_hash_ref);



my($bhash_ref,$bHoA,$bAoA,$bHoL) = proc_open_files("Process Budget Entries",$budget_file);
#print Dumper \$bhash_ref;
print Dumper \$bHoA;
#print Dumper \$bAoA;
#print Dumper \$bHoL;
print_hash_ref_line("Printing Budget bHoA",$bHoA);


my($hash_ref,$HoA,$AoA,$HoL) = proc_open_files("Process Entries",$entries_file);
#print Dumper \$hash_ref;
print Dumper \$HoA;
#print Dumper \$AoA;
#print Dumper \$HoL;

print_hash_ref_line("Printing HreF",$HoA);
compare_mk_file("Final Report",$header_ref,$HoA,$bHoA,$entry_hash_ref,$bhash_ref);


exit;


##################################################################33
my $hash_ref = open_files("Budget",$budget_file);
my $entry_hash_ref = open_files("Entries",$entries_file);

print_hash("Balance",$entry_hash_ref);

open_detail_budget("Budget Detail",$budget_file);
print "\n";

# Subs
#
sub get_date{
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) =
                                                localtime(time);
$year += 1900;
$mon += 1;
$mday = sprintf("%02d", $mday);
my $date = $year."-".$mon."-".$mday;
return ($date);
};

sub compare_mk_file{
my($title,$header_ref,$HoA,$bHoA,$entry_hash_ref,$bhash_ref) = @_;
print "-----< $title >---------\n";
my $ii = 0;
my $fybo_tot =0;
my $fyba_tot = 0;
my $total = 0;
my $atotal = 0;
my $stotal = 0;
my $btotal = 0;    
###

###
my $outfile = "pc_budget_status_file_";
my $fdate = get_date();
$outfile = $outfile."-".$fdate."_.csv";
open ( my $fh, ">", $outfile) || die "Flaming death on open of: $outfile: $?\n\n";

print "#,Budget Entry,Name,2020 Rec,2020 ADJ,2019 Rec,2019 ADJ,Spent/Enc,Balance,GL,Type,Note\n";
print $fh "#,Budget Entry,Name,2020 Rec,2020 ADJ,2019 Rec,2019 ADJ,Spent/Enc,Balance,GL,Type,Note\n";


foreach my $key ( nsort keys %{ $bHoA } ){
       $ii++;
       my $lnum = $key;
       my $fybo = ${$bHoA}{$key}[0][1];
       my $fyba = ${$bHoA}{$key}[0][2];
       my $name = ${$bHoA}{$key}[0][6];
       my $desc = ${$bHoA}{$key}[0][7];
       chomp($desc);
       my $oamt = ${$bHoA}{$key}[0][3];
       my $aamt = ${$bHoA}{$key}[0][4];      
       my $gl = ${$bHoA}{$key}[0][5];
       my $type = ${$bHoA}{$key}[0][8];
       chomp($type);
       my $spent;
        if ( ${$entry_hash_ref}{$key} ) {
              $spent = ${$entry_hash_ref}{$key};
            } else {
              $spent = 0;
           };
       my $bal = $aamt - $spent;
       print "$ii,$lnum,$name,$fybo,$fyba,$oamt,$aamt,$spent,$bal,$gl,$type,$desc\n";
       print $fh "$ii,$lnum,$name,$fybo,$fyba,$oamt,$aamt,$spent,$bal,$gl,$type,$desc\n";
         $fybo_tot += $fybo;
         $fyba_tot += $fyba;
         $total += $oamt;
         $atotal += $aamt;
         $stotal += $spent; 
         $btotal += $bal;
#         print_hash_ref_line("Printing HreF",$HoA);     
          foreach my $aa ( 0 .. $#{ ${$HoA}{$key}} ){
                             print ", , , , , , , , , , , ,";
                             print $fh ", , , , , , , , , , , ,";
                          my @array;
             foreach my $kk ( 0 .. $#{ ${$HoA}{$key}[$aa] } ){
                             # print "[$ii][$kk]:";
                              #print ",${$HoA}{$key}[$aa][$kk]";
                              #print $fh ",${$HoA}{$key}[$aa][$kk]";
                             push @array, ${$HoA}{$key}[$aa][$kk];
                  };
                 my $lf = $array[0];
                 my $epmt = $array[1];
                 my $enote;
                 if (  $array[5] ) { $enote = $array[5] } else { $enote = "-" };
                 my $paid = $array[4];
                 my $ename = $array[3];
                 print "  XXXX what's going on?   ";               
                 print "$lf-$ename, $epmt, $paid - $enote\n";
                 print $fh "$lf-$ename, $epmt, $paid - $enote\n";
             };
  };
            print "Totals, , ,$fybo_tot, $fyba_tot, $total, $atotal, $stotal, $btotal\n";
            print $fh "Totals, , ,$fybo_tot, $fyba_tot, $total, $atotal, $stotal, $btotal\n";

            close $fh;
};

###

###


sub print_hash {
# sub to print hash
my($title,$hashref) = @_;
my $ii = 0;
my $total = 0;
print "----< $title >--------\n";
for my $entry ( sort keys %{ $hashref } ){
   $ii++;
   print "$ii: $entry = ${$hashref}{$entry}\n";
   $total += ${$hashref}{$entry};
 }
   print "Total: $total\n";
   print "\n\n";
};

sub open_detail_budget {
my($title,$file) = @_;
print "\n----< $title >--------\n";
my %hash = ();
my $total = 0;
my $ii = 0;
open ( my $fh, "<", $file ) || die "Flaming death: $?\n";
while (<$fh>){
    my $line = $_;
    if ( $line =~ m/^#+/ ) { #print "Skipping header.\n"; 
                              next; };
    chomp($line);
    #print "\$line: $line\n";
    my($tag,$note,$amount) = split(/\,/,$line);
     $ii++;
     print "$ii: $tag = $note, $amount\n";
     $total += $amount;
     if( $hash{$tag} ){
         $hash{$tag} .= $note.",".$amount;
        } else {
     $hash{$tag} = $note.",".$amount;
     }
    }
     print "Total = $total\n\n";
  return(\%hash);
}

###
sub proc_open_files {
my($title,$file) = @_;
print "----< $title >--------\n";
my %hash = ();
my %HoA = ();
my %HoL = ();
my @array = ();
my @AoA = ();
my $total = 0;
my $ii = 0;
my $headerlength;
open ( my $fh, "<", $file ) || die "Flaming death: $?\n";
while (<$fh>){
    my $line = $_;
    chomp($line);
    if ( $line =~ m/^#+/ ) { 
       my(@headers) = split(/\,/,$line);
       $headerlength = $#headers;
       print "### Header is $headerlength long.\n";
         #print "Skipping header.\n"; 
                              next; 
       };
    #chomp($line);
    #print "\$line: $line\n";
    my($tag,$amount,$gl,$desc,$note) = split(/\,/,$line);
    my @array = split(/\,/,$line);
     $ii++;
     
     print "$ii: $tag = $amount\n";
     $total += $amount;
     if( $hash{$tag} ){
         $hash{$tag} += $amount;
        } else {
     $hash{$tag} = $amount;
     }
     ###
     if( $HoL{$tag} ){
         $HoL{$tag} .= $amount;
        } else {
     $HoL{$tag} = $amount;
     }
     ###
     if( $HoA{$tag} ){
          print "\$HoA{$tag} Exists!\n";
          push $HoA{$tag}, [ @array ];
             } else {
                  print "\$HoA{$tag} Does not Exist yet.  Making.\n";
                  $HoA{$tag} = [];
                  push $HoA{$tag}, [ @array ];
                  #print Dumper \%HoA;
          };      
     
     ###

     ###
     push @AoA, [ @array ];
     ###
    }
     print "Total = $total\n\n";
  return(\%hash,\%HoA,\@AoA,\%HoL);
}

###
sub print_hash_ref{
my($title,$href) = @_;
print "-------< $title >-------\n";
foreach my $tag ( nsort keys %{ $href } ){
    print "\$tag: $tag\n";
     foreach my $ii ( 0 .. $#{ ${$href}{$tag} } ){
            #    print "[$ii]";
          foreach my $kk ( 0 .. $#{ ${$href}{$tag}[$ii] } ){
                 print "[$ii][$kk]:";
                 print "${$href}{$tag}[$ii][$kk]\n";
                };
               };
             };
};

###
sub print_hash_ref_line{
my($title,$href) = @_;
print "-------< $title >-------\n";
foreach my $tag ( nsort keys %{ $href } ){
    print "\$tag: $tag\n";
     foreach my $ii ( 0 .. $#{ ${$href}{$tag} } ){
               #print "[$ii]:";
          foreach my $kk ( 0 .. $#{ ${$href}{$tag}[$ii] } ){
                 #print "[$ii][$kk]:";
                 # print "[$kk]:${$href}{$tag}[$ii][$kk]";
                   print ",${$href}{$tag}[$ii][$kk]";
                };
                 print "\n";
               };
                 print "\n";
             };
};
###
sub open_files_one_col {
my($title,$file) = @_;
print "----< $title >--------\n";
my %hash = ();
my $total = 0;
my $ii = 0;
my @header;
open ( my $fh, "<", $file ) || die "Flaming death: $?\n";
while (<$fh>){
    my $line = $_;
    chomp($line);
    if ( $line =~ m/^#+/ ) { 
                             #print "Skipping header.\n"; 
                             @header = split(/\,+/,$line); 
                             print "### Header has $#header fields.\n";
                              next; };
    #print "\$line: $line\n";
    my($tag,$amount,$gl,$desc,$note) = split(/\,/,$line);
     $ii++;
     print "$ii: $tag = $amount\n";
     $total += $amount;
     if( $hash{$tag} ){
         $hash{$tag} += $amount;
        } else {
     $hash{$tag} = $amount;
     }
    }
     print "Total = $total\n";
    print "----< end $title >----\n\n";
  return(\%hash);
}

###
sub open_files_two_col {
my($title,$file) = @_;
print "----< $title >--------\n";
my %hash = ();
my $total = 0;
my $ii = 0;
my @header;
open ( my $fh, "<", $file ) || die "Flaming death: $?\n";
while (<$fh>){
    my $line = $_;
    chomp($line);
    if ( $line =~ m/^#+/ ) { 
                             #print "Skipping header.\n"; 
                             @header = split(/\,+/,$line); 
                             print "### Header has $#header fields.\n";
                              next; };
    #print "\$line: $line\n";
    my($tag,$fybo,$fyba,$org,$amount,$gl,$desc,$note) = split(/\,/,$line);
     $ii++;
     print "$ii: $tag = $amount\n";
     $total += $amount;
     if( $hash{$tag} ){
         $hash{$tag} += $amount;
        } else {
     $hash{$tag} = $amount;
     }
    }
     print "Total = $total\n";
    print "----< end $title >----\n\n";
  return(\%hash,\@header);
}

